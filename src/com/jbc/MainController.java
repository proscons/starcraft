package com.jbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MainController {
	public static void main(String[] args) {
		List<Unit> arrUnit = new ArrayList<Unit>();
		Map<Integer, Unit> mapUnit = new HashMap<Integer, Unit>();
		Scanner scanner = new Scanner(System.in);
		int j = 0;
		while (true) {

			printGuide();
			int input = scanner.nextInt();
			if (input == 1) {
				j = j + 1;
				makeUnit(arrUnit, mapUnit, j, scanner);
			} else if (input == 2) {
				showInformation(arrUnit, scanner);
			} else if (input == 3) {
				int i = 0;
				showUnit(arrUnit, i);
				while (true) {
					try {

						int putin = scanner.nextInt();

						Unit a = arrUnit.get(putin);
						a.move();
						break;
					} catch (Exception a) {
						System.out.println("존재하지 않은 유닛을 선택했습니다. 존재하는 유닛을 선택하세요.");

					}
				}
			} else if (input == 4) {
				attack(arrUnit, scanner);
			} else if (input == 5) {
				heal(arrUnit, scanner);
			} else if (input == 6) {
				upgrade(arrUnit, scanner);
			} else if (input == 9) {
				arrUnit.clear();
				j = 0;
				System.out.println("유닛들이 초기화 되었습니다.");
			} else if (input == 0) {
				break;
			}
		}
		System.out.println("종료되었습니다.");

	}

	private static void printGuide() {
		System.out.println("1=유닛 생성하기");
		System.out.println("2=유닛 정보 보기");
		System.out.println("3=유닛 움직이기");
		System.out.println("4=공격하기");
		System.out.println("5=치료하기");
		System.out.println("6=훈련하기");
		System.out.println("9=초기화");
		System.out.println("0=끝내기");
	}

	private static void makeUnit(List<Unit> arrUnit, Map<Integer, Unit> mapUnit, int j, Scanner scanner) {
		System.out.println("마린을 생성할꺼면 1 , 파이어뱃을 생성할꺼면 2");

		int id = (int) (Math.random() * 100000);
		int putin = scanner.nextInt();
		if (putin == 1) {

			Marine m = new Marine();
			m.name = j + " 번 마린";
			m.Id = id;
			System.out.println("id가 " + id + "인 " + m.name + "이 생성되었습니다.");
			arrUnit.add(m);
			mapUnit.put(id, m);
		} else if (putin == 2) {

			Firebat f = new Firebat();
			f.name = j + " 번 파이어벳";
			f.Id = id;
			System.out.println("id가 " + id + "인 " + f.name + "이 생성되었습니다.");
			arrUnit.add(f);
			mapUnit.put(id, f);

		}
	}

	private static void showInformation(List<Unit> arrUnit, Scanner scanner) {
		if (arrUnit.isEmpty()) {
			System.out.println("생성된 유닛이 없습니다. 유닛을 생성하세요.");
		} else {
			int i = 0;
			for (Unit u : arrUnit) {
				System.out.println(i + " 을 누르면 " + u.Id + " 번 " + u.name + " 의 정보가 보입니다.");
				i++;
			}
			while (true) {
				try {
					int Input = scanner.nextInt();
					Unit u = arrUnit.get(Input);
					System.out.println("번호 : " + u.name + " 체력 : " + u.hp + " 데미지 : " + u.damage + " x 좌표: "
							+ u.getLocation().getX() + " y 좌표 : " + u.getLocation().getY());
					break;
				} catch (Exception a) {
					System.out.println("존재하지 않은 유닛을 선택했습니다. 존재하는 유닛을 선택하세요.");

				}
			}
		}
	}

	private static void showUnit(List<Unit> arrUnit, int i) {
		for (Unit m : arrUnit) {
			System.out.println(i + "을 누르면 " + m.Id + "번 마린이 선택됩니다.");
			i++;
		}
	}

	private static void attack(List<Unit> arrUnit, Scanner scanner) {
		System.out.println("공격할 유닛을 선택하시오.");
		int i = 0;
		for (Unit m : arrUnit) {
			System.out.println(i + "을 누르면 " + m.Id + "번 유닛이 선택됩니다.");
			i++;
		}
		while (true) {
			try {
				int Input = scanner.nextInt();
				Unit m1 = arrUnit.get(Input);
				System.out.println(m1.Id + "번 마린이 선택됬습니다.");
				i = 0;
				System.out.println("공격대상 마린을 선택하시오.");
				showUnit(arrUnit, i);
				int putin = scanner.nextInt();
				Unit m2 = arrUnit.get(putin);
				System.out.println(m2.Id + "번 마린을 공격합니다.");
				m1.hit(m2);
				if (m2.hp <= 0) {
					System.out.println(m2.Id + "번 마린이 죽었습니다.");
					arrUnit.remove(m2);
				}
				break;
			} catch (Exception a) {
				System.out.println("존재하지 않은 유닛을 선택했습니다. 존재하는 유닛을 선택하세요.");
			}
		}
	}

	private static void heal(List<Unit> arrUnit, Scanner scanner) {
		if (arrUnit.isEmpty()) {
			System.out.println("생성된 병사가 없습니다. 병사를 생성하세요.");
		}
		System.out.println("치료 할 마린을 선택하시오.");
		int i = 0;
		for (Unit m : arrUnit) {
			System.out.println(i + "을 누르면 " + m.Id + "번 마린이 선택됩니다.");
			i++;

			while (true) {
				try {
					int p = scanner.nextInt();

					Unit u = arrUnit.get(p);
					u.heal();
					break;
				} catch (Exception a) {
					System.out.println("존재하지 않은 유닛을 선택했습니다. 존재하는 유닛을 선택하세요.");
				}
			}
		}
	}

	private static void upgrade(List<Unit> arrUnit, Scanner scanner) {
		if (arrUnit.isEmpty()) {
			System.out.println("생성된 마린이 없습니다. 마린을 생성하세요.");
		}
		System.out.println("체력을 업그레이드를 시킬꺼면 1번, 공격력을 업그레이드를 시킬꺼면 2번");
		int b = scanner.nextInt();
		if (b == 1) {
			System.out.println("체력 업그레이드를 할 마린을 선택하시오.");
			int i = 0;
			showUnit(arrUnit, i);
			while (true) {
				try {
					int p = scanner.nextInt();

					Unit m = arrUnit.get(p);
					m.upgradehp();
					System.out.println(m.Id + "번 마린의 체력 업그레이드가 완료되었습니다.");
					break;
				} catch (Exception a) {
					System.out.println("존재하지 않은 유닛을 선택했습니다. 존재하는 유닛을 선택하세요.");
				}
			}
		}else if (b==2) {
			while (true) {
				try {
					System.out.println("공격력 업그레이드를 할 마린을 선택하시오.");
					int i = 0;
					showUnit(arrUnit, i);
					int p = scanner.nextInt();
					Unit m = arrUnit.get(p);
					m.upgradedamage();
					System.out.println(m.Id + "번 마린의 공격력 업그레이드가 완료되었습니다.");
					break;
				} catch (Exception a) {
					System.out.println("존재하지 않은 유닛을 선택했습니다. 존재하는 유닛을 선택하세요.");
				}
			}
		}
	}

}
