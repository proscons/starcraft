package com.jbc;

import java.util.Scanner;

public class Unit {

	Scanner scanner = new Scanner(System.in);
	protected String name;
	protected int splashDamage;
	protected int maxhp = 80;
	protected int heal = 20;
	protected int Id;
	protected int range;
	protected Location location = new Location();
	protected int hp;
	protected int damage;
	
	
	public Unit(int range, int hp, int damage) {
		this.range = range;
		this.damage = damage;
		this.hp = hp;
	}
	
	void move() {
		System.out.println("좌표값을 입력하세요 x : ");
		this.getLocation().setX(scanner.nextInt());
		System.out.println("좌표값을 입력하세요 y : ");
		this.getLocation().setY(scanner.nextInt());
	}
	
	public void hit(Unit target) {
		if(isAttackable(target)) {
			target.hp = target.hp - this.damage;
		} else {
			System.out.println("사정거리가 안닿음.");
		}
	}
	
	public boolean isAttackable(Unit target) {
		Location from = this.getLocation();
		Location to = target.getLocation();
		
		double distance = from.getDistance(to);
		
		if (distance < range) {
			return true;
		} else {
			return false;
		}
	}

	void upgradehp() {

		this.hp = this.hp + 10;
		this.maxhp = this.maxhp + 10;
	}

	void heal() {
		this.hp = this.hp + heal;
		if (this.hp > maxhp) {
			this.hp = maxhp;
		}
	}

	void upgradedamage() {
		this.damage = this.damage + 10;
	}

	public Scanner getScanner() {
		return scanner;
	}

	public void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}

	public int getSplashDamage() {
		return splashDamage;
	}

	public void setSplashDamage(int splashDamage) {
		this.splashDamage = splashDamage;
	}

	

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getMaxhp() {
		return maxhp;
	}

	public void setMaxhp(int maxhp) {
		this.maxhp = maxhp;
	}

	public int getHeal() {
		return heal;
	}

	public void setHeal(int heal) {
		this.heal = heal;
	}

}
